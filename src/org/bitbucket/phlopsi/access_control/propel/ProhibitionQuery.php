<?php
namespace org\bitbucket\phlopsi\access_control\propel;

use org\bitbucket\phlopsi\access_control\propel\Base\ProhibitionQuery as BaseProhibitionQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'prohibitions' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProhibitionQuery extends BaseProhibitionQuery
{
    
}

// ProhibitionQuery
