<?php
namespace org\bitbucket\phlopsi\access_control\propel;

use org\bitbucket\phlopsi\access_control\propel\Base\RolesSessionTypesQuery as BaseRolesSessionTypesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'roles_session_types' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RolesSessionTypesQuery extends BaseRolesSessionTypesQuery
{
    
}

// RolesSessionTypesQuery
